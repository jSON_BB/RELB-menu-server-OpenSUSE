#!/bin/bash
#
# Install the LawBreakers Menu Server
#
AUTHOR="jSON"
VERSION="V20211214.1212"
POWER=16
function check_sudo {
        echo "Check sudo rights..."
        timeout 2 sudo -v && sudo="true" || sudo="false"
        if [ "${sudo}" == "false" ] ; then
                echo "No root privileges. Exiting..."
                echo "Please run this with a privileged user."
                exit 1
        fi
        echo "Access granted"
}
function dependencies_opensuse {
        echo "#################################"
        echo "Install Dependencies for OpenSUSE"
        echo "#################################"
        sudo zypper in wget curl gpg2 net-tools nodejs git screen
        sudo rpm --import http://repo.mongodb.org/zypper/suse/15/mongodb-org/4.4/x86_64/repodata/repomd.xml.asc
        sudo zypper ref
        sudo zypper ar -f http://repo.mongodb.org/zypper/suse/15/mongodb-org/4.4/ MongoDB-org
        sudo zypper in mongodb-org
}
function prep_database {
        echo "################"
        echo "Prepare Database"
        echo "################"

        #sudo service mongodb start
        if [ "${NAME}" == "OpenSUSE Tumbleweed" ] ; then
                sudo systemctl start mongod
        fi
        # Generate Random Char String
        if [ "x${SECRET}" == "x" ] ; then
                SECRET=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${POWER} | head -n 1)
        fi
        echo "use RELB-menu-database
db.dropDatabase()
use RELB-menu-database
db.createCollection(\"servers\")
db.servers.insert({secret: \"${SECRET}\", region: \"EU\"})" | mongo
        echo "${SECRET}" > ${PWD}/secret.txt
        sudo chmod 0600 ${PWD}/secret.txt
}
function git_clone {
        echo "##################"
        echo "Clone Server files"
        echo "##################"
        echo "LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KYjNCbGJuTnphQzFyWlhrdGRqRUFB
QUFBQkc1dmJtVUFBQUFFYm05dVpRQUFBQUFBQUFBQkFBQUFNd0FBQUF0emMyZ3RaVwpReU5UVXhP
UUFBQUNEY1U3TXErMVFNbXdGSmw1MEcwaGJxUThBV0VMY1l2RCtKWm44L0ViWDArQUFBQUtDcy9D
cVpyUHdxCm1RQUFBQXR6YzJndFpXUXlOVFV4T1FBQUFDRGNVN01xKzFRTW13RkpsNTBHMGhicVE4
QVdFTGNZdkQrSlpuOC9FYlgwK0EKQUFBRURpUG1wSGFNT1FtYmgyMWRoRUtUYmVwZGxTWmN2VTNN
dmx3UXBMZWg3eFh0eFRzeXI3VkF5YkFVbVhuUWJTRnVwRAp3QllRdHhpOFA0bG1mejhSdGZUNEFB
QUFHVzE1ZEdoaWRXSmliR1ZBWjI5dloyeGxiV0ZwYkM1amIyMEJBZ01FCi0tLS0tRU5EIE9QRU5T
U0ggUFJJVkFURSBLRVktLS0tLQo=" | base64 -d > ${PWD}/priv_key
        sudo chmod 0600 ${PWD}/priv_key
        eval "$(ssh-agent -s)"
        ssh-add ./priv_key
        rm -rf "${PWD}/priv_key"
        P_FILE2="~/.ssh/known_hosts"
        if  [ -f "${P_FILE2}" ] ; then
                if [ ! -n "$(grep "^github.com " ~/.ssh/known_hosts)" ]; then
                        echo "Add Github SSH Fingerprint to Known Hosts File..."
                        ssh-keyscan github.com >> ~/.ssh/known_hosts 2>/dev/null
                fi
        else
                mkdir -p ~/.ssh/
                touch ~/.ssh/known_hosts
                if [ ! -n "$(grep "^github.com " ~/.ssh/known_hosts)" ]; then
                        echo "Add Github SSH Fingerprint to Known Hosts File..."
                        ssh-keyscan github.com >> ~/.ssh/known_hosts 2>/dev/null
                fi
        fi
        git clone git@github.com:The-RELB-Project/relb-server.git
        mv ${PWD}/secret.txt relb-server/secret.txt
        if [ $P_CONFIG == "true" ] ; then
                mv ${PWD}/config.js relb-server/config.js
        fi
        cd relb-server
        npm install
        npm audit fix
        sudo chmod u+x setup.sh
}
function start_menu_server {
        echo "###############"
        echo "Starting Server"
        echo "###############"
        if [ "x${DUSER}" == "x" ]; then
                screen -dmS menu-server bash -c 'npm run linux ; exec bash'
                echo "Your Secret is: ${SECRET} this is used for the game-server"
                echo "it is located under: ${PWD}/secret.txt"
                echo "Detached the menu-server Screen. You can attach it with:"
                echo "screen -x menu-server"
        else
                screen -dmS menu-server bash -c "sudo su ${DUSER} -c 'cd /home/${DUSER}/relb-server/ ; npm run linux ; exec bash'"
                echo "Your Secret is: ${SECRET} this is used for the game-server"
                echo "it is located under: /home/${DUSER}/relb-server/secret.txt"
                echo "Detached the menu-server Screen. You can attach it with:"
                echo "sudo su - ${DUSER}"
                echo "screen -x menu-server"
        fi
}
function ask_uninstall_user {
        if [ "${P_FORCE}" == "true" ] ; then
                if [ "${P_CONFIG}" == "true" ] ; then
                        cp /home/${DUSER}/relb-server/config.js ./
                fi
                sudo rm -rf /home/${DUSER}/relb-server/
                # kill screen
                sudo su - $DUSER -c 'screen -r -S "menu-server" -X quit 2>/dev/null'
        else
                read -p "Remove your current install? (y/N)" -n 1 -r
                if [[ $REPLY =~ ^[Yy]$ ]] ; then
                        P_FORCE="true"
                        ask_uninstall_user
                else
                        echo "Invalid input. Exiting..."
                        exit 1
                fi
        fi
}
function ask_uninstall_default {
        if [ "${P_FORCE}" == "true" ] ; then
                if [ "${P_CONFIG}" == "true" ] ; then
                        cp ${PWD}/relb-server/config.js ./
                fi
                sudo rm -rf ${PWD}/relb-server/
                # kill screen
                screen -r -S "menu-server" -X quit 2>/dev/null
        else
                read -p "Remove your current install? (y/N)" -n 1 -r
                if [[ $REPLY =~ ^[Yy]$ ]] ; then
                        P_FORCE="true"
                        ask_uninstall_default
                else
                        echo "Invalid input. Exiting..."
                        exit 1
                fi
        fi
}
function check_install {
        echo "Check install..."
        # port 8080 in use?
        NETSTAT_OUTPUT=$(netstat -lntp4 | grep 8080)
        if [ "x${NETSTAT_OUTPUT}" != "x" ] ; then
                echo "aborting port 8080 is in use!"
                exit 1
        fi
        # folder exists?
        if [ "x${DUSER}" != "x" ] ; then
                DIR="/home/${DUSER}/relb-server"
                if  [ -d "${DIR}" ] ; then
                        ask_uninstall_user
                fi
        else
                DIR="${PWD}/relb-server"
                if  [ -d "${DIR}" ] ; then
                        ask_uninstall_default
                fi
        fi
}
function no_deps {
        check_install || exit 1
        prep_database || exit 1
        git_clone || exit 1
        start_menu_server || exit 1
}
function full_install_opensuse {
        check_install || exit 1
        dependencies_opensuse || exit 1
        prep_database || exit 1
        git_clone || exit 1
        start_menu_server || exit 1
}
function add_user {
        sudo useradd -m ${DUSER}
        mv relb-server /home/${DUSER}
        chown -R ${DUSER}:${DUSER} /home/${DUSER}
}
function remove_me {
        if [ "x${DUSER}" == "x" ] ; then
                ask_uninstall_default
        else
                ask_uninstall_user
        fi
}
function check_distro {
        P_FILE="/etc/os-release"
        if  [ -f "${P_FILE}" ] ; then
        . /etc/os-release
                if [ "${NAME}" == "OpenSUSE Tumbleweed" ] ; then
                        if [ "${P_INSTALL}" == "true"   ] ; then
                                full_install_opensuse
                        fi
                        if [ "${P_NODEPS}" == "true"   ] ; then
                                no_deps
                        fi
                fi
        else
                echo "/etc/os-release not exist! Exiting..."
                exit 1
        fi
}
function print_help {
        echo -e "\t"
        echo -e "\t${0} -i / --install                   # Install menu-server with dependencies."
        echo -e "\t${0} -d / --skipdeps                  # Install menu-server without checking for dependencies."
        echo -e "\t${0} -u / --user ([string] USERNAME)  # Change install user. (also adds /home/\$USER) Default is current user"
        echo -e "\t${0} -p / --power ([INT] POWER)       # Change default strength for token gen. Default is 16"
        echo -e "\t${0} -c / --config                    # Save currently active config."
        echo -e "\t${0} -s / --secret                    # Predefine secret."
        echo -e "\t${0} -f / --force                     # Does not show promts. (combine with -i / -d or -r)"
        echo -e "\t${0} -r / --remove                    # Uninstall. (pass -u if you created a user for it!)"
        echo -e "\t${0} -x / --debug                     # Add debug output. (set -x)"
        echo -e "\t${0} -v / --version                   # Print Version."
        echo -e "\t${0} -h / --help                      # Print help page."
        echo -e "\t"
        echo -e "\tWritten by ${AUTHOR}"
        echo -e "\t"
}
function print_version {
        echo "Version: ${VERSION}"
}
function sort_args {
        if [ "${P_DEBUG}" == "true" ] ; then
                set -x
        fi
        if [ "${P_HELP}" == "true" ] ; then
                print_help
        fi
        if [ "${P_VERSION}" == "true" ] ; then
                print_version
        fi
        if [ "${P_REMOVE}" == "true" ] ; then
                remove_me
        fi
        if [ "${P_NODEPS}" == "true" ] ; then
                check_distro
        fi
        if [ "${P_INSTALL}" == "true" ] ; then
                check_distro
        fi
        if [ "${P_DUSER}" == "true" ] ; then
                add_user
        fi
}
if [ $# -gt 0 ] ; then
        while [ $# -gt 0 ] ; do
                case $1 in
                        -i | --install) P_INSTALL="true" ;;
                        -d | --skipdeps) P_NODEPS="true" ;;
                        -u | --user)  P_DUSER="true" ; DUSER=$2 ;;
                        -p | --power) POWER=$2 ;;
                        -c | --config) P_CONFIG="true" ;;
                        -f | --force) P_FORCE="true" ;;
                        -r | --remove) P_REMOVE="true" ;;
                        -s | --secret) SECRET=$2 ;;
                        -x | --debug) P_DEBUG="true" ;;
                        -v | --version) print_version ;;
                        -h | --help) print_help ;;
                esac
                shift
        done
        sort_args
else
        print_help
fi
exit 0
